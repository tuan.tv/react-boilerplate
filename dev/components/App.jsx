import React from 'react'
import { Button } from 'react-bootstrap'

export default class App extends React.Component {
    render() {
        return (
            <div className='text-center'>
                <h1>Hello React</h1>
            </div>
        )
    }
}
